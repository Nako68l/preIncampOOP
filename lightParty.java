import java.util.Arrays;
import java.util.List;

interface illuminatingMechanism {
    void switchOn();
}

class Projector implements illuminatingMechanism {
    String currentImg = "Rabbit";

    @Override
    public void switchOn() {
        showImg();
    }

    private void showImg() {
        System.out.println("Showing " + currentImg + " on the wall");
    }
}

class Lighter implements illuminatingMechanism {
    private int brightness = 5;
    private int MAX_LIGHTER_BRIGHTNESS = 10;
    private int MIN_LIGHTER_BRIGHTNESS = 0;

    void increaseLightPower(){
        if (brightness < MAX_LIGHTER_BRIGHTNESS) brightness++;
        else System.out.println("Max lvl of brightness is reached");
    }

    void decreaseLightPower(){
        if (brightness > MIN_LIGHTER_BRIGHTNESS) brightness--;
        else System.out.println("Min lvl of brightness is reached");
    }

    @Override
    public void switchOn() {
        System.out.println("Lights up the room with " + brightness + " level of brightness");
    }
}

class scaredHomeAlone {
    static private List<illuminatingMechanism> allLightDevicesIHave = Arrays.asList(new Lighter(), new Projector());

    static void switchOnAllLightDevicesIHave(){
        allLightDevicesIHave.forEach(illuminatingMechanism::switchOn);
    }

    public static void main(String[] args) {
        switchOnAllLightDevicesIHave();
    }
}
